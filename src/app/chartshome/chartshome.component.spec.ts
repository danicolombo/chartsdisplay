import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartshomeComponent } from './chartshome.component';

describe('ChartshomeComponent', () => {
  let component: ChartshomeComponent;
  let fixture: ComponentFixture<ChartshomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartshomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartshomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
